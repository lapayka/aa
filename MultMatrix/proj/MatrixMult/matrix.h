#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <ostream>
#include <fstream>
#include <initializer_list>

using namespace std;

template <typename Type>
class Matrix
{
private:
    int _n, _m;
    Type **matrix;
public:
    Matrix() = default;
    Matrix(int n, int m);
    Matrix(const initializer_list<initializer_list<Type>> &list);
    ~Matrix();

    void setSize(int n, int m);

    int n() {return _n; }
    int m() {return _m; }

    Type &operator ()(int i, int j) {return matrix[i][j]; };
    Type* operator [](int i){return matrix[i]; };

    Type *getArray() {return matrix[0]; };
};

template<typename Type>
void Matrix<Type>::setSize(int n, int m)
{
    _n = n;
    _m = m;
    matrix = nullptr;
    matrix = new Type*[n];
    *matrix = nullptr;
    *matrix = new Type[n * m];

    for (int i = 1; i < n; i++)
        matrix[i] = matrix[i - 1] + m;
}

template<typename t>
Matrix<t>::Matrix(const initializer_list<initializer_list<t>> &list)
{
    _n = list.size();
    _m = (list.begin())->size();
    matrix = nullptr;
    matrix = new t*[_n];
    *matrix = nullptr;
    *matrix = new t[_n * _m];

    for (int i = 1; i < _n; i++)
        matrix[i] = matrix[i - 1] + _m;

    int* arr = *matrix;

    for (const auto & _list : list)
    {
        for (const auto & elem : _list)
            *arr++ = elem;
    }
}



template<typename Type>
ostream & operator<< (ostream & stream, Matrix<Type> & m)
{
    for(int i = 0; i < m.n(); i++)
    {
        for(int j = 0; j < m.m(); j++)
            stream << m[i][j] <<" ";
        stream << "\n";
    }
    return stream;
}

template<typename Type>
istream & operator>> (istream & stream, Matrix<Type> & mat)
{
    int n, m;
    stream >> n >> m;
    mat.setSize(n, m);
    for(int i = 0; i < mat.n(); i++)
    {
        for(int j = 0; j < mat.m(); j++)
            stream >> mat[i][j];
    }
    return stream;
}

template<typename Type>
Matrix<Type>::Matrix(int n, int m)
    :
      _n(n),
      _m(m)
{
    matrix = nullptr;
    matrix = new Type*[n];
    *matrix = nullptr;
    *matrix = new Type[n * m];

    for (int i = 1; i < n; i++)
        matrix[i] = matrix[i - 1] + m;
}

template<typename Type>
Matrix<Type>::~Matrix()
{
    if (matrix != nullptr)
    {
        if (*matrix != nullptr)
            delete [] *matrix;
        delete [] matrix;
        matrix = nullptr;
    }
}

#endif // MATRIX_H
