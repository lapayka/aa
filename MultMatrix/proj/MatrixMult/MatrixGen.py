from random import *

sizes = [i for i in range(10, 101, 10)]

alphabet = "sdfghjklqwertyuiopzxcvbnm"

file = open("..\\arrays.txt", "w")

for i in sizes:
    matrix1 = []
    matrix2 = []
    for j in range(i):
        arr1 = []
        arr2 = []
        for k in range (i):
            arr1.append(randint(0, 50))
            arr2.append(randint(0, 50))
        matrix1.append(arr1)
        matrix2.append(arr2)

    file.write(str(i) + " " + str(i) + "\n")
    for j in range(i):
        for k in range (i):
            if k != i-1:
                file.write(str(matrix1[j][k]) + " ")
            else:
                file.write(str(matrix1[j][k]) + "\n")

    file.write(str(i) + " " + str(i) + "\n")
    for j in range(i):
        for k in range (i):
            if k != i-1:
                file.write(str(matrix2[j][k]) + " ")
            else:
                file.write(str(matrix2[j][k]) + "\n")
file.close()
