import matplotlib.pyplot as plt

file = open("..\\times.txt", "r")

sizes = [i for i in range(10, 101, 10)]

clas = list(map(float, file.readline().split()))
vin = list(map(float, file.readline().split()))
ovin = list(map(float, file.readline().split()))

plt.plot(sizes, clas, label = "Классический алгоритм")
plt.plot(sizes, vin, label = "Алгоритм Винограда")
plt.plot(sizes, ovin, label = "Оптимизированный алгоритм Винограда")
plt.xlabel("Линейный размер матрицы")
plt.ylabel("Время (мс)")
plt.legend()

plt.show()
    
file.close()


