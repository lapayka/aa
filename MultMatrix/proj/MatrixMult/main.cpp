#include <iostream>
#include <string>

#include <time.h>
#include <assert.h>
#include "matrix.h"

enum {ARR_COUNT = 10};

using namespace std;

//template<typename type>
void class_mult(Matrix<int> &out,  Matrix<int> &left,  Matrix<int> &right)
{
    if (left.m() != right.n())
        return;

    for (int i = 0; i < left.n(); i++)
        for (int j = 0; j < right.m(); j++)
        {
            int tmp = 0;
            for (int k = 0; k < left.m(); k++)
            {
                tmp += left[i][k] * right[k][j];
            }
            out[i][j] = tmp;
        }
}

//template<typename int>
void vinograd_mult(Matrix<int> &out,  Matrix<int> &left,  Matrix<int> &right)
{
    if (left.m() != right.n())
        return;

    int *row_factor = (int*)malloc(left.n() * sizeof(int));
    int *col_factor = (int*)malloc(right.m() * sizeof(int));

    int d = left.m() / 2;

    for (int i = 0; i < left.n(); i++)
    {
        row_factor[i] = 0;
        for (int j = 0; j < d; j++)
            row_factor[i] += left[i][2 * j] * left[i][2 * j + 1];
    }
    for (int i = 0; i < right.m(); i++)
    {
        col_factor[i] = 0;
        for (int j = 0; j < d; j++)
            col_factor[i] += right[2*j][i] * right[2 * j + 1][i];
    }

    for (int i = 0; i < left.n(); i++)
        for (int j = 0; j < right.m(); j++)
        {
            out[i][j] = -row_factor[i] - col_factor[j];
            //cout << out;
            for (int k = 0; k < d; k++)
            {
                out[i][j] += (left[i][2*k] + right[2*k+1][j]) * (left[i][2*k+1] + right[2*k][j]);
            }
            //cout << out;
        }
    if (left.m() % 2)
        for (int i = 0; i < left.n(); i++)
            for (int j = 0; j < right.m(); j++)
                out[i][j] += left[i][left.m() - 1] * right[right.n() - 1][j];
    free(col_factor);
    free(row_factor);
}

//template<typename int>
void op_vinograd_mult(Matrix<int> &out,  Matrix<int> &left,  Matrix<int> &right)
{
    if (left.m() != right.n())
        return;

    int *row_factor = (int*)malloc(left.n() * sizeof(int));
    int *col_factor = (int*)malloc(right.m() * sizeof(int));

    for (int i = 0; i < left.n(); i++)
    {
        row_factor[i] = col_factor[i] = 0;
        for (int j = 0; j < left.m() - 1; j += 2)
        {
            row_factor[i] += left[i][j] * left[i][j + 1];
            col_factor[i] += right[j][i] * right[j + 1][i];
        }
    }

    for (int i = 0; i < left.n(); i++)
        for (int j = 0; j < right.m(); j++)
        {
            int res = -row_factor[i] - col_factor[j];
            for (int k = 0; k < left.m() - 1; k += 2)
            {
                res += (left[i][k] + right[k+1][j]) * (left[i][k+1] + right[k][j]);
            }
            out[i][j] = res;
        }
    if (left.m() % 2)
        for (int i = 0; i < left.n(); i++)
            for (int j = 0; j < right.m(); j++)
                out[i][j] += left[i][left.m() - 1] * right[right.n() - 1][j];



    free(col_factor);
    free(row_factor);
}

//template<typename type>
double timeMeasure(Matrix<int> &left,  Matrix<int> &right, void (*mult)(Matrix<int> &, Matrix<int> &, Matrix<int> &))
{
    int measurmentsCount = 10000;

    Matrix<int> out(left.n(), right.m());

    clock_t sTime = clock();

    for (int i = 0; i < measurmentsCount; i++)
    {
        mult(out, left, right);
    }

    clock_t eTime = clock();


    return (double)(eTime - sTime) / measurmentsCount ;
}

void make_test(Matrix<int> &A, Matrix<int> &B)
{
    {
        Matrix<int> m1(A.n(), B.m());
        cout << "Test matrix 1:\n" << A;
        cout << "Test matrix 2:\n" << B;
        class_mult(m1, A, B);
        cout << "Classic:\n" << m1;
        vinograd_mult(m1, A, B);
        cout << "Vinograd:\n" << m1;
        op_vinograd_mult(m1, A, B);
        cout << "Optimized Vinograd:\n" << m1;
        cout << "-----------\n";
    }
}

void make_tests()
{
    setbuf(stdout, NULL);

    Matrix<int> test_mat1({{1, 2, 3}});
    Matrix<int> test_mat2({{4}, {5}, {6}});
    Matrix<int> test_mat3({{1}, {2}, {3}});
    Matrix<int> test_mat4({{4, 5, 6}});
    Matrix<int> test_mat5({{2}});
    Matrix<int> test_mat6({{1, 2, 3}, {2,4,1}, {5,2,3}});
    Matrix<int> test_mat7({{-1, 2, 2}, {4,2,2}, {3,2,3}});

    make_test(test_mat1, test_mat2);
    make_test(test_mat3, test_mat4);
    make_test(test_mat5, test_mat5);
    make_test(test_mat6, test_mat7);
}

void outputArray(ostream &stream, double arr[], int size)
{
    for (int i = 0; i < size; i++)
        stream << arr[i] << " ";
    stream << "\n";
}


int main()
{
    make_tests();

    Matrix<int> sampling[20];

    ifstream istream("..\\arrays.txt");


    if (istream.is_open())
    {
        for (int i = 0; i < 20; i++)
            istream >> sampling[i];

        for (int i = 0; i < 20; i++)
        {
            cout << sampling[i];
            cout<<"------";
        }

        istream.close();

        double classic[10];
        double vinograd[10];
        double opvinograd[10];

        for (int i = 0; i < 10; i++)
        {
            classic[i] = timeMeasure(sampling[2 * i], sampling[2 * i + 1], class_mult);
            vinograd[i] = timeMeasure(sampling[2 * i], sampling[2 * i + 1], vinograd_mult);
            opvinograd[i] = timeMeasure(sampling[2 * i], sampling[2 * i + 1], op_vinograd_mult);
        }

        ofstream ostream("..\\times.txt");

        outputArray(ostream, classic, 10);
        outputArray(ostream, vinograd, 10);
        outputArray(ostream, opvinograd, 10);


        ostream.close();
    }



    return 0;
}
