import matplotlib.pyplot as plt

file = open("times.txt", "r")

sizes = [i for i in range(10, 51, 5)]

notp = list(map(float, file.readline().split()))
first = list(map(float, file.readline().split()))
second = list(map(float, file.readline().split()))
third = list(map(float, file.readline().split()))
p = list(map(float, file.readline().split()))

for i in range(len(notp)):
    notp[i] /= 3

plt.plot(sizes, p, label = "Параллельный конвейер")
plt.plot(sizes, notp, label = "Последовательная реализация")
plt.xlabel("Количество заявок")
plt.ylabel("Время(c)")
plt.legend()

plt.show()
    
file.close()


