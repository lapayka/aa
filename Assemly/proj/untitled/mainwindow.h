#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <thread>
#include <queue>
#include <memory>
#include <QPixmap>
#include <QBitmap>
#include <QGraphicsScene>
#include <fstream>

#include "cwavegenerator.h"
#include "cpoligoncreator.h"
#include "zbuffer.h"
#include <QTime>
#include <QImage>
#include <mutex>
#include "sys/time.h"

using namespace std;

struct logger
{
    double gen[9];
    double pol[9];
    double buf[9];
    int i = 0;
};

struct genApp
{
    shared_ptr<HeightMap> hmap;
    double velocity;
    double time;

    genApp(const shared_ptr<HeightMap> &_hmap, double v, double t) : hmap(_hmap), velocity(v), time(t) {}
};

struct polApp
{
    shared_ptr<HeightMap> hmap;
    Point light;
    double angle;

    polApp(const shared_ptr<HeightMap> &_hmap, const Point &_light, double a) : hmap(_hmap), light(_light), angle(a) {}
};

struct bufApp
{
    shared_ptr<Poligons> pols;
    int pol_size;
    Point light;

    bufApp(const Point &_light, const shared_ptr<Poligons> & _pols, int size) : pols(_pols), light(_light), pol_size(size) {}
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void draw(Color *colors[]);

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
};

#endif // MAINWINDOW_H
