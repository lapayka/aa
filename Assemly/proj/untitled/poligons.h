#ifndef POLIGONS_H
#define POLIGONS_H

#include <memory>
#include "point.h"
#include <iostream>


using namespace std;

struct Poligon
{
    Point normal;
    double D;
    Point A, B, C;
    Poligon() = default;

    Poligon (const Point &_A, const Point &_B, const Point &_C) {A = _A; B= _B; C = _C; }
    Poligon (const Point &_A, const Point &_B, const Point &_C, const Point &n) {A = _A; B= _B; C = _C; normal = n; }
    Poligon (const Point &_A, const Point &_B, const Point &_C, const Point &n, double _D) {A = _A; B= _B; C = _C; normal = n; D = _D; }

    void setD (int _D) {D = _D;}
};

class Poligons
{
    int n;
    Point max;
    Point min;
    shared_ptr<Poligon[]> arr;

    Point light;
public:
    Poligons(int size) :
        n(size),
        arr(new Poligon[size])
    {}
    int size() {return n; }
    shared_ptr<Poligon[]> array() {return arr; }

    void setLight(const Point &_light) {light = _light;}
    const Point& getLight() {return light; }

    void setMax(const Point &point) {max = point; }
    void setMin(const Point &point) {min = point; }
    Point& getMax() {return max; }
    Point& getMin() {return min; }
};

std::ostream & operator << (ostream &stream, const Poligon &pol);

#endif // POLIGONS_H
