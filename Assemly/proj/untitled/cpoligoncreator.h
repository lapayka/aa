#ifndef CPOLIGONCREATOR_H
#define CPOLIGONCREATOR_H

#include "poligons.h"
#include "heightmap.h"

class CPoligonCreator
{
public:
    CPoligonCreator() = default;
    ~CPoligonCreator() = default;

    shared_ptr<Poligons> getPoligons(const shared_ptr<HeightMap> &hMap, const Point &light,  double angle);
};

#endif // CPOLIGONCREATOR_H
