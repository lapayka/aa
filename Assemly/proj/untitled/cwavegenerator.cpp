#include "cwavegenerator.h"

#include "cwavegenerator.h"
#include <iostream>

enum {WAVES_COUNT = 8, RAND_COUNT = 12};
#define EPS 1e-6
#define A 0.5
#define g 9.8

static inline double generateNormal()
{
    double r = 0.0;
    for (int i = 0; i < RAND_COUNT; i ++)
        r += (2.0 * rand()) / RAND_MAX;

    return r / RAND_COUNT - 1;
}

static inline double Ph(const Point &k, double velocity)
{
    double k_2 = k.sqr();
    double L = velocity * velocity / g;

    if (fabs(k_2) < EPS)
        return 0.0;

    return A / (k_2 * k_2) * k.y * k.y * exp(-1.0 / (k_2 * L * L));
}

static inline complex<double> h(const Point &k, double velocity)
{
    double Ei = generateNormal(), Ej = generateNormal();

    return (0.25 / (sqrt(2.0))) * complex<double>(Ei, Ej) * sqrt(Ph(k, velocity));
}

static inline void generateSpectr(const shared_ptr<HeightMap> &hMap, double velocity)
{
    Matrix<complex<double>> &mat = *(hMap->h0.get());
    double Lx = hMap->Lx;
    double Lz = hMap->Lz;

    int n_2 = mat.n()/2;
    int m_2 = mat.m()/2;

    #pragma omp parallel for(16)
    for (int i = -n_2; i < n_2; i++)
        for (int j = -m_2; j < m_2; j++)
        {
            Point k(2.0 * M_PI * i / Lx, 2.0 * M_PI * j / Lz);
            mat(i + n_2, j + m_2) = h(k, velocity);
        }
    //setbuf(stdout, NULL);
    //for (int i = 0; i < mat.n(); i++)
    //{
    //    for (int j = 0; j < mat.m(); j++)
    //        cout << mat(i,j) << " ";
    //    cout << "\n";
    //}
    //cout << "----" << "\n";

}

static inline void reCountAmplitudes(const shared_ptr<HeightMap> &hMap, double t)
{
    Matrix<complex<double>> &mat = *(hMap->h0.get());
    double Lx = hMap->Lx;
    double Lz = hMap->Lz;

    int n_2 = mat.n()/2;
    int m_2 = mat.m()/2;

    int n = mat.n() - 1;
    int m = mat.m() - 1;

    #pragma omp parallel for(16)
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
        {
            Point k(2.0 * M_PI * (i - n_2) / Lx, 2.0 * M_PI * (j - m_2) / Lz);

            double w = sqrt(double(k) * g);

            mat(i,j) *= exp(complex<double>(0.0, w * t));
        }
}

static inline void generateHeightMap(const shared_ptr<HeightMap> &hMap)
{
    Matrix<complex<double>> &h_mat = *(hMap->h.get());
    Matrix<complex<double>> &h0_mat = *(hMap->h0.get());
    double Lx = hMap->Lx;
    double Lz = hMap->Lz;

    double mPer_nodex = Lx / h_mat.n();
    double mPer_nodey = Lz / h_mat.m();

    int n_2 = h_mat.n()/2;
    int m_2 = h_mat.m()/2;

    #pragma omp parallel for(16)
    for (int i = 0; i < h_mat.n(); i++)
        for (int j = 0; j < h_mat.m(); j++)
        {
            complex<double> h;
            Point p(mPer_nodex * (i - n_2), mPer_nodey * (j - m_2));
            for (int ix = 0; ix < h_mat.n(); ix++)
                for (int jy = 0; jy < h_mat.m(); jy++)
                {
                    Point k(2.0 * M_PI * (ix - n_2) / Lx, 2.0 * M_PI * (jy - m_2) / Lz);

                    h += h0_mat(ix,jy) * exp(complex<double>(0.0, k * p));;
                    //cout << h << " ";
                }
            h_mat(i,j) = h;
        }
    //for (int i = 0; i < h_mat.n(); i++)
    //{
    //    for (int j = 0; j < h_mat.m(); j++)
    //        cout << h_mat(i,j) << " ";
    //    cout << "\n";
    //}
    //cout << "----" << "\n";

}

void CWaveGenerator::generateMaps(const shared_ptr<HeightMap> &hMap, double time, double velocity)
{
    if (hMap->first_time)
    {
        generateSpectr(hMap, velocity);
        hMap->first_time = false;
    }
    else
    {
        reCountAmplitudes(hMap, time);
    }

    generateHeightMap(hMap);
}
