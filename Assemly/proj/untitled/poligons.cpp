#include "poligons.h"

std::ostream & operator << (ostream &stream, const Poligon &pol)
{
    stream << "[" << pol.A << pol.B << pol.C << "] ";

    return stream;
}
