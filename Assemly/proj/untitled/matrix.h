#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

template <typename Type>
class Matrix
{
private:
    int _n, _m;
    Type **matrix;
public:
    Matrix() = default;
    Matrix(int n, int m);
    Matrix(const Matrix &matrix);
    ~Matrix();

    int n() {return _n; }
    int m() {return _m; }

    Type &operator ()(int i, int j) {return matrix[i][j]; }
    Type* operator [](int i){return matrix[i]; }

    Type *getArray() {return matrix[0]; }
};

template<typename Type>
Matrix<Type>::Matrix(int n, int m)
    :
      _n(n),
      _m(m)
{
    matrix = nullptr;
    matrix = new Type*[n];
    *matrix = nullptr;
    *matrix = new Type[n * m];

    for (int i = 1; i < n; i++)
        matrix[i] = matrix[i - 1] + m;
}

template<typename Type>
Matrix<Type>::Matrix(const Matrix &_matrix)
    : _n(_matrix.n),
      _m(_matrix.m)
{
    matrix = nullptr;
    matrix = new Type*[_n];
    *matrix = nullptr;
    *matrix = new Type[_n * _m];

    memcpy(matrix[0], _matrix.matrix[0], sizeof (Type) * _n * _m);

    for (int i = 1; i < _n; i++)
        matrix[i] = matrix[i - 1] + _m;
}

template<typename Type>
Matrix<Type>::~Matrix()
{
    if (matrix != nullptr)
    {
        if (*matrix != nullptr)
            delete [] *matrix;
        delete [] matrix;
        matrix = nullptr;
    }
}

#endif // MATRIX_H
