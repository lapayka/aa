#ifndef CWAVEGENERATOR_H
#define CWAVEGENERATOR_H

#include <memory>
#include "heightmap.h"
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include "point.h"

using namespace std;

class CWaveGenerator
{
public:
    CWaveGenerator() = default;
    void generateMaps(const shared_ptr<HeightMap> &hMap, double time, double velocity);
};
#endif // CWAVEGENERATOR_H
