#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    setbuf(stdout, NULL);
    w.show();

    return a.exec();
}
