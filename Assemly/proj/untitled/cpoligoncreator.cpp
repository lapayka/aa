#include "cpoligoncreator.h"

#include "float.h"
#include <stdarg.h>

#define EPS 1e-6

double min(int n, ...)
{
    va_list list;
    va_start(list, n);

    double min = DBL_MAX;
    double tmp;
    for (int i = 0; i < n; i++)
        if ((tmp = va_arg(list, double)) < min)
            min = tmp;

    va_end(list);
    return min;
}

double max(int n, ...)
{
    va_list list;
    va_start(list, n);

    double max = -DBL_MAX;
    double tmp;
    for (int i = 0; i < n; i++)
        if ((tmp = va_arg(list, double)) > max)
            max = tmp;

    va_end(list);
    return max;
}

void rotate_scale(Point &point, double angle, double k)
{
    double y = point.y;
    point.y = point.y * cos(angle) - point.z * sin(angle);
    point.z = (y * sin(angle) - point.z * cos(angle)) * k;
    point.x = point.x * k;
    point.y = -point.y * k + BaseDrawManager::WINDOWHEIGHT / 2;
}

shared_ptr<Poligons> CPoligonCreator::getPoligons(const shared_ptr<HeightMap> &hMap, const Point &_light,  double angle)
{
    Matrix<complex<double>> &mat = *(hMap->h.get());

    shared_ptr<Poligons> pol(new Poligons((mat.n() - 1) * (mat.m() - 1) * 2));


    Point light = _light;
    //rotate_scale(light, angle, 1.0);
    //light.y = BaseDrawManager::WINDOWHEIGHT / 2 -  light.y;
    pol->setLight(light);

    //setbuf(stdout, NULL);

    Poligon *arr = pol->array().get();


    double y_min = DBL_MAX;
    double y_max = -DBL_MAX;

    double x_step = hMap->Lx / mat.n();
    double z_step = hMap->Lz / mat.m();
    double k = BaseDrawManager::WINDOW_WIDTH / (hMap->Lx - x_step);
    double cur_z = -hMap->Lx / 2;

    #pragma omp parallel for
    for (int i = 0; i < mat.m() - 1; i++)
    {
        double cur_x = 0;
        for (int j = 0; j < mat.n() - 1; j++)
        {
            Point a(cur_x, mat(j,i).real(), cur_z);
            Point b(cur_x + x_step, mat(j+1, i).real(), cur_z);
            Point c(cur_x + x_step, mat(j + 1, i + 1).real(), cur_z + z_step);
            Point d(cur_x, mat(j, i + 1).real(), cur_z + z_step);

            rotate_scale(a, angle, k);
            rotate_scale(b, angle, k);
            rotate_scale(c, angle, k);
            rotate_scale(d, angle, k);

            Point n1((c - a).vector_prod(d - a));
            n1 = n1 * (1.0 / (double(n1)));

            Point n2((b - a).vector_prod(c - a));
            n2 = n2 * (1.0 / (double(n2)));

            *arr++ = Poligon(a, d, c, n1, -n1 * a);
            *arr++ = Poligon(a, b, c, n2, -n2 * a);
            cur_x += x_step;

        }

        cur_z += z_step;
    }



    //for (int i = 0; i < pol->size(); i++)
    //    cout << min(3, carr[i].A.z, arr[i].B.z, arr[i].C.z) << "\n";

    pol->setMin(Point(0, y_min, 0));
    pol->setMax(Point(BaseDrawManager::WINDOW_WIDTH, y_max, 0.0));

    return pol;
}

