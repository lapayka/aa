#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <semaphore.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(this);
    ui->Scene->setScene(scene);
    ui->Scene->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    scene->setSceneRect(0, 0, ui->Scene->width(), ui->Scene->height());
}

MainWindow::~MainWindow()
{
    delete ui;
}

#define ARR_SIZE 10
#define TARR_SIZE 50

std::mutex mtx;
sem_t semaphores[2];



logger logg;

void sleepFeature()
{
    QTime end = QTime::currentTime().addMSecs(100);
    while (QTime::currentTime() < end)
    {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }
    return;
}

void tassem_gen(queue<genApp> *queue, std::queue<polApp> *queue2, int n)
{
    while (!queue->empty())
    {
        time_t start = clock();
        genApp app = queue->back();
        queue->pop();

        CWaveGenerator().generateMaps(app.hmap, app.time, app.velocity);

        mtx.lock();
        queue2->push(polApp(app.hmap, Point(0,1,0), M_PI / 6.0));
        mtx.unlock();
        logg.gen[logg.i] += clock() - start;
    }
}

void tassem_pol(queue<polApp> *queue, std::queue<bufApp> *queue2, int n)
{

    for (int i = 0; i < n;)
    {
        if (!queue->empty())
        {
            time_t start = clock();
            polApp app = queue->back();
            mtx.lock();
            queue->pop();
            mtx.unlock();

            shared_ptr<Poligons> pols = CPoligonCreator().getPoligons(app.hmap, app.light, app.angle);

            bufApp bapp(Point(0,1,0), pols, pols->size());

            mtx.lock();
            queue2->push(bapp);
            mtx.unlock();

            i++;
            logg.pol[logg.i] += clock() - start;
        }
    }

}

void tassem_buf(std::queue<bufApp> *queue, Color **colors, int n)
{

    for (int i = 0; i < n;)
    {
        if (!queue->empty())
        {
            time_t start = clock();
            bufApp app = queue->back();

            mtx.lock();
            queue->pop();
            mtx.unlock();

            Color *color = (Color *)calloc(BaseDrawManager::WINDOWHEIGHT * BaseDrawManager::WINDOW_WIDTH, sizeof (Color));
            ZBuffer().draw(color, BaseDrawManager::WINDOWHEIGHT, BaseDrawManager::WINDOW_WIDTH, app.pols->array().get(), app.pol_size , app.light);
            colors[i] = color;
            i++;
            logg.buf[logg.i] += clock() - start;
        }
    }

}

void assem_gen(queue<genApp> *queue, std::queue<polApp> *queue2, int n)
{
    while (!queue->empty())
    {
        genApp app = queue->back();
        queue->pop();

        CWaveGenerator().generateMaps(app.hmap, app.time, app.velocity);

        mtx.lock();
        queue2->push(polApp(app.hmap, Point(0,1,0), M_PI / 6.0));
        mtx.unlock();
        sem_post(&semaphores[0]);
    }
}

void assem_pol(queue<polApp> *queue, std::queue<bufApp> *queue2, int n)
{

    for (int i = 0; i < n;)
    {
        if (sem_trywait(&semaphores[0]) && !queue->empty())
        {
            polApp app = queue->back();
            mtx.lock();
            queue->pop();
            mtx.unlock();

            shared_ptr<Poligons> pols = CPoligonCreator().getPoligons(app.hmap, app.light, app.angle);

            bufApp bapp(Point(0,1,0), pols, pols->size());

            mtx.lock();
            queue2->push(bapp);
            mtx.unlock();
            sem_post(&semaphores[1]);

            i++;
        }
    }
}

void assem_buf(std::queue<bufApp> *queue, Color **colors, int n)
{
    for (int i = 0; i < n;)
    {
        if (sem_trywait(&semaphores[1]) && !queue->empty())
        {
            bufApp app = queue->back();
            mtx.lock();
            queue->pop();
            mtx.unlock();

            Color *color = (Color *)calloc(BaseDrawManager::WINDOWHEIGHT * BaseDrawManager::WINDOW_WIDTH, sizeof (Color));
            ZBuffer().draw(color, BaseDrawManager::WINDOWHEIGHT, BaseDrawManager::WINDOW_WIDTH, app.pols->array().get(), app.pol_size , app.light);
            colors[i] = color;
            i++;

        }
    }
}


void MainWindow::on_pushButton_clicked()
{
    Color *ccolors[ARR_SIZE];
    cout << sizeof(Color);
    double time = 0;;
    for (int i = 0; i < ARR_SIZE; i++)
    {
        shared_ptr<HeightMap> h(new HeightMap(64, 64, 20, 20));

        CWaveGenerator().generateMaps(h, time, 2.0);
        shared_ptr<Poligons> pols = CPoligonCreator().getPoligons(h, Point(0, 1, 0), M_PI / 6);

        Color *colors = (Color *)calloc(BaseDrawManager::WINDOW_WIDTH * BaseDrawManager::WINDOWHEIGHT, sizeof(Color));

        ZBuffer().draw(colors, BaseDrawManager::WINDOWHEIGHT, BaseDrawManager::WINDOW_WIDTH, pols->array().get(), pols->size(), Point(0, 1, 0));
        ccolors[i] = colors;

        time += 0.1;

    }

    draw(ccolors);
    for(int i = 0; i < ARR_SIZE; i++)
        free(ccolors[i]);
}

void MainWindow::on_pushButton_2_clicked()
{
    std::queue<genApp> queue1;
    std::queue<polApp> queue2;
    std::queue<bufApp> queue3;

    Color *ccolors[ARR_SIZE];

    shared_ptr<HeightMap> maps[ARR_SIZE];
    for (int i = 0; i < ARR_SIZE; i++)
        maps[i] = shared_ptr<HeightMap>(new HeightMap(64, 64, 20, 20));

    double time = -0.1;
    for (int i = 0; i < ARR_SIZE; i++)
        queue1.push(genApp(maps[i], 2.0, time+=0.1));

    sem_init(&semaphores[0], 0, ARR_SIZE);
    sem_init(&semaphores[1], 0, ARR_SIZE);

    std::thread th1(assem_gen, &queue1, &queue2, ARR_SIZE);
    std::thread th2(assem_pol, &queue2, &queue3, ARR_SIZE);
    std::thread th3(assem_buf, &queue3, ccolors, ARR_SIZE);

    th1.join();
    th2.join();
    th3.join();

    draw(ccolors);

}

void MainWindow::draw(Color *colors[])
{
    for (int i = 0; i < ARR_SIZE; i++)
    {
        QImage im((uchar *)colors[i], BaseDrawManager::WINDOW_WIDTH, BaseDrawManager::WINDOWHEIGHT, QImage::Format_RGB888);
        scene->clear();
        scene->addPixmap(QPixmap::fromImage(im));
        scene->update();
        sleepFeature();
    }
}



void MainWindow::on_pushButton_3_clicked()
{
    Color *ccolors[TARR_SIZE];
    std::queue<genApp> queue1;
    std::queue<polApp> queue2;
    std::queue<bufApp> queue3;
    shared_ptr<HeightMap> maps[TARR_SIZE];
    for (int i = 0; i < TARR_SIZE; i++)
        maps[i] = shared_ptr<HeightMap>(new HeightMap(32, 32, 20, 20));

    ofstream os("times.txt");


    memset(&logg, 0, sizeof(logg));

    struct timeval start;
    struct timeval end;
    for (int j = 10; j <= TARR_SIZE; j += 5)
    {
        for (int i = 0; i < j; i++)
            queue1.push(genApp(maps[i], 2.0, 0.0));

        time_t start = clock();

        std::thread th1(tassem_gen, &queue1, &queue2, j);
        std::thread th2(tassem_pol, &queue2, &queue3, j);
        std::thread th3(tassem_buf, &queue3, ccolors, j);

        th1.join();
        th2.join();
        th3.join();

        time_t end = clock();

        os << double(end - start) << " ";
        logg.i += 1;
    }
    os << "\n";

    for (int i = 0; i < (TARR_SIZE - 10) / 5 + 1; i++)
        os << logg.gen[i] << " ";
    os << "\n";

    for (int i = 0; i < (TARR_SIZE - 10) / 5 + 1; i++)
        os << logg.pol[i] << " ";
    os << "\n";

    for (int i = 0; i < (TARR_SIZE - 10) / 5 + 1; i++)
        os << logg.buf[i] << " ";
    os << "\n";

    for (int j = 10; j <= TARR_SIZE; j += 5)
    {
        for (int i = 0; i < j; i++)
            queue1.push(genApp(maps[i], 2.0, 0.0));

        time_t start = clock();

        assem_gen (&queue1, &queue2, j);
        assem_pol (&queue2, &queue3, j);
        assem_buf (&queue3, ccolors, j);

        time_t stop = clock();

        os << (double)(stop - start) << " ";
    }
    os << "\n";

    os.close();



}
