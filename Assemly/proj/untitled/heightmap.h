#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include "matrix.h"
#include <memory>
#include <complex.h>

using namespace std;

struct HeightMap
{
    int n, m;
    double Lx, Lz;
    bool first_time;
    shared_ptr<Matrix<double>> d;
    shared_ptr<Matrix<complex<double>>> h0;
    shared_ptr<Matrix<complex<double>>> h;

    HeightMap() = default;
    HeightMap(int _n, int _m, double _Lx, double _Lz) :
        n(_n),
        m(_m),
        Lx(_Lx),
        Lz(_Lz),
        first_time(true),
        d(new Matrix<double>(n, m)),
        h0(new Matrix<complex<double>>(n, m)),
        h(new Matrix<complex<double>>(n, m))
    {}
};

#endif // HEIGHTMAP_H
