#ifndef ZBUFFER_H
#define ZBUFFER_H

#include "poligons.h"

#define EPS 1e-6

struct Color
{
    unsigned char R;
    unsigned char G;
    unsigned char B;

    Color() = default;
    Color(unsigned int A, unsigned int _B, unsigned int C) {R = A; G = _B; B = C; }
    Color(const Color &c) {R = c.R; G = c.G; B = c.B; }


    Color operator + (Color rhs) {rhs.R += R; rhs.G += G; rhs.B += B; return rhs; }
    Color operator * (double num) {Color tmp(*this); tmp.R *= num; tmp.G *= num; tmp.B *= num; return tmp; }
};

ostream & operator << (ostream & cout, const Color &color);


class ZBuffer
{
public:
    ZBuffer() = default;

    void draw(Color *colors, int n, int m, Poligon *pols, int pol_size, const Point &light);
};

#endif // ZBUFFER_H
