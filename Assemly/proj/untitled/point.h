#ifndef POINT_H
#define POINT_H

#include <math.h>
#include <iostream>
using namespace std;

struct BaseDrawManager
{
    enum {WINDOWHEIGHT = 800, WINDOW_WIDTH = 1000 };
};

struct Point
{
    double x, y, z;
    public:
        Point();
        Point(double x, double y)
        {
            this->x = x;
            this->y = y;
            z = 0.0;
        }
        Point(double x, double y, double z)
        {
            this->x = x;
            this->y = y;
            this->z = z;
        }
        Point operator + (Point point) const {point.x += x; point.y += y; point.z += z; return point;}
        Point operator - (Point point) const {point.x = x - point.x; point.y = y - point.y; point.z = z - point.z; return point;}
        Point operator * (double num) const {Point buf(*this); buf.x *= num; buf.y *= num; buf.z *= num; return buf; }
        Point operator - () const {Point buf(-x, -y,-z); return buf; }
        double operator * (const Point &point) const {return x * point.x + y * point.y + z *point.z; }

        Point vector_prod (const Point &B) const { return Point(y * B.z - z * B.y,
                                                                -(x * B.z - z * B.x),
                                                                x * B.y - y * B.x);}

        operator double() const {return sqrt(x * x + y * y + z * z); }
        double sqr() const {return x * x + y * y + z * z; }

        Point &operator = (const Point &_point) {x = _point.x; y = _point.y; z = _point.z; return (*this); }
};

std::ostream & operator << (ostream &stream, const Point &point);


#endif // POINT_H
