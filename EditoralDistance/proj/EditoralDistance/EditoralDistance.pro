TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        editoradistancealgoritms.cpp \
        main.cpp \
        matrix.cpp

HEADERS += \
        editoradistancealgoritms.h \
        matrix.h
