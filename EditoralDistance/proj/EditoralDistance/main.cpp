#include <iostream>
#include <string>

#include "editoradistancealgoritms.h"
#include <time.h>
#include <assert.h>

using namespace std;

double timeMeasure(const char *s1, const char *s2, int (*editDistance)(const char *, const char *))
{
    int measurmentsCount = 10000;

    clock_t sTime = clock();

    for (int i = 0; i < measurmentsCount; i++)
    {
        editDistance(s1, s2);
    }

    clock_t eTime = clock();


    return (double)(eTime - sTime) / measurmentsCount ;
}

void make_tests()
{
    const char *s1 = (const char*)"ckat";
    const char *s2 = (const char*)"kot";
    const char *s3 = (const char*)"abc";
    const char *s4 = (const char*)"def";

    assert(DistanceWithCache(s1,s2) == 2);
    assert(DistanceWithCache(s3,s4) == 3);
    assert(DistanceWithCache(s3,s3) == 0);

    assert(recursiveDistanceWithCache(s1,s2) == 2);
    assert(recursiveDistanceWithCache(s3,s4) == 3);
    assert(recursiveDistanceWithCache(s3,s3) == 0);

    assert(recursiveDistance(s1,s2) == 2);
    assert(recursiveDistance(s3,s4) == 3);
    assert(recursiveDistance(s3,s3) == 0);

    assert(recursiveDamerauDistance(s1,s2) == 2);
    assert(recursiveDamerauDistance(s3,s4) == 3);
    assert(recursiveDamerauDistance(s3,s3) == 0);

    const char *s5 = (const char*)"kcta";
    assert(recursiveDamerauDistance(s1,s5) == 2);
}


int main()
{

    make_tests();

    string s1[10];
    string s2[10];

    ifstream istr("strings.txt");
    if (istr.is_open())
    {
    for (int i = 0; i < 10; i++)
        istr >> s1[i] >> s2[i];

    istr.close();

    int (*(funcs[4]))(const char *, const char *) = {DistanceWithCache, recursiveDistance, recursiveDistanceWithCache, recursiveDamerauDistance};

    ofstream ostr("times.txt");
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            if ((i != 1 && i != 3) || j < 1)
            {
                cout << i << " " << j << "\n";
                ostr << timeMeasure(s1[j].c_str(), s2[j].c_str(), funcs[i]) << " ";
            }
        }
        ostr << "\n";
    }
    ostr.close();
    }
    return 0;
}
