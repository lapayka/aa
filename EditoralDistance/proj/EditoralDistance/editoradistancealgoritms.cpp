#include "editoradistancealgoritms.h"

int DistanceWithCache(const char * s1, const char * s2)
{
    int lenM = strlen(s2) + 1;
    int lenN = strlen(s1) + 1;

    int *fString = (int *)malloc(lenM * sizeof(int));
    int *sString = (int *)malloc(lenM * sizeof(int));
    for (int i = 0; i < lenM; i++)
        fString[i] = i;


    for (int i = 1; i < lenN; i++)
    {
        sString[0] = i;
        for (int j = 1; j < lenM; j++)
        {
            int j_1 = j - 1;
            int min = fString[j] + 1;
            int buf = sString[j_1] + 1;
            if (buf < min)
                min = buf;
            buf =  fString[j_1] +  (int)(s1[i - 1] != s2[j_1]);
            if (buf < min)
                min = buf;

            sString[j] = min;
        }
        swap(fString, sString);
    }

    int retValue = fString[lenM - 1];

    free(fString);
    free(sString);

    return retValue;
}

static int _recursiveDistance(const char * s1, const char * s2, int lenN, int lenM)
{
    if (lenN == 0)
        return lenM;
    if (lenM == 0)
        return lenN;

    int min = _recursiveDistance(s1, s2, lenN, lenM - 1) + 1;
    int buf;
    if ((buf = _recursiveDistance(s1, s2, lenN - 1, lenM) + 1) < min)
        min = buf;
    buf = _recursiveDistance(s1, s2, lenN - 1, lenM - 1) + (s1[lenN - 1] != s2[lenM - 1]);
    if (buf < min)
        min = buf;

    return min;
}

int recursiveDistance(const char * s1, const char * s2)
{
    return _recursiveDistance(s1, s2, strlen(s1), strlen(s2));
}

static int _recursiveDamerauDistance(const char * s1, const char * s2, int lenN, int lenM)
{
    if (lenN == 0)
        return lenM;
    if (lenM == 0)
        return lenN;

    int min = _recursiveDamerauDistance(s1, s2, lenN, lenM - 1) + 1;
    int buf;
    if ((buf = _recursiveDamerauDistance(s1, s2, lenN - 1, lenM) + 1) < min)
        min = buf;
    buf = _recursiveDamerauDistance(s1, s2, lenN - 1, lenM - 1) + int(s1[lenN - 1] != s2[lenM - 1]);
    if (buf < min)
        min = buf;

    if (lenN > 1 && lenM > 1)
    {
        if (s1[lenN-1] == s2[lenM-2] && s1[lenN-2] == s2[lenM - 1])
            if ((buf = _recursiveDamerauDistance(s1, s2, lenN-2, lenM - 2) + 1) < min)
                min = buf;
    }

    return min;
}

int recursiveDamerauDistance(const char * s1, const char * s2)
{
    return _recursiveDamerauDistance(s1, s2, strlen(s1), strlen(s2));
}

static int _recursiveDistanceWithCache(const char * s1, const char * s2, int lenN, int lenM, Matrix<int> & mat)
{
    int &elem = mat(lenN, lenM);

    //cout << mat << lenN << " " << lenM << "\n";

    if (elem == -1)
    {
        int min = _recursiveDistanceWithCache(s1, s2, lenN - 1, lenM - 1, mat) +
                                                          (s1[lenN - 1] != s2[lenM - 1]);
        int buf;
        if ((buf = _recursiveDistanceWithCache(s1, s2, lenN - 1, lenM, mat) + 1) < min)
            min = buf;
        if ((buf = _recursiveDistanceWithCache(s1, s2, lenN, lenM - 1, mat) + 1) < min)
            min = buf;

        return (elem = min);
    }
    return elem;
}

int recursiveDistanceWithCache(const char * s1, const char * s2)
{
    int lenN = strlen(s1);
    int lenM = strlen(s2);

    Matrix<int> mat(lenN + 1, lenM + 1);

    for (int i = 0; i < lenM + 1; i++)
        mat[0][i] = i;

    for (int i = 0; i < lenN + 1; i++)
        mat[i][0] = i;

    for (int i = 1; i < lenN + 1; i++)
        for (int j = 1; j < lenM + 1; j++)
            mat[i][j] = -1;

    //cout << mat;



    return _recursiveDistanceWithCache(s1, s2, lenN, lenM, mat);
}
