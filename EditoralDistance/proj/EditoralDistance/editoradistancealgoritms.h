#ifndef EDITORADISTANCEALGORITMS_H
#define EDITORADISTANCEALGORITMS_H

#include "matrix.h"
#include <iostream>
#include <string.h>


int DistanceWithCache(const char* s1, const char* s2);

int recursiveDistance(const char* s1, const char * s2);

int recursiveDamerauDistance(const char* s1, const char * s2);

int recursiveDistanceWithCache(const char * s1, const char * s2);


#endif // EDITORADISTANCEALGORITMS_H
