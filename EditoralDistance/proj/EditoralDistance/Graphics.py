import matplotlib.pyplot as plt

file = open("..\\times.txt", "r")

sizes = [i for i in range(5, 51, 5)]

dist = list(map(float, file.readline().split()))
rec = list(map(float, file.readline().split()))
recCahce = list(map(float, file.readline().split()))

plt.plot(sizes, dist, label = "Нерекурсивная реализация с кэшем")
plt.plot(sizes, recCahce, label = "Рекурсивная реализация с кешем")
plt.xlabel("Размер строк")
plt.ylabel("Время")
plt.legend()

plt.show()
    
file.close()


