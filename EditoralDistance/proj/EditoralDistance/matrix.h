#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>
#include <ostream>
#include <fstream>

using namespace std;

template <typename Type>
class Matrix
{
private:
    int _n, _m;
    Type **matrix;
public:
    Matrix(int n, int m);
    ~Matrix();

    int n() {return _n; }
    int m() {return _m; }

    Type &operator ()(int i, int j) {return matrix[i][j]; };
    Type* operator [](int i){return matrix[i]; };

    Type *getArray() {return matrix[0]; };
};

template<typename Type>
ostream & operator<< (ostream & stream, Matrix<Type> & m)
{
    for(int i = 0; i < m.n(); i++)
    {
        for(int j = 0; j < m.m(); j++)
            stream << m[i][j] <<" ";
        stream << "\n";
    }
    return stream;
}

template<typename Type>
Matrix<Type>::Matrix(int n, int m)
    :
      _n(n),
      _m(m)
{
    matrix = nullptr;
    matrix = new Type*[n];
    *matrix = nullptr;
    *matrix = new Type[n * m];

    for (int i = 1; i < n; i++)
        matrix[i] = matrix[i - 1] + m;
}

template<typename Type>
Matrix<Type>::~Matrix()
{
    if (matrix != nullptr)
    {
        if (*matrix != nullptr)
            delete [] *matrix;
        delete [] matrix;
        matrix = nullptr;
    }
}

#endif // MATRIX_H
