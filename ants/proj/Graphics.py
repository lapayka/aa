import matplotlib.pyplot as plt

file = open("times.txt", "r")

sizes = [i for i in range(3, 11, 1)]

dist = list(map(float, file.readline().split()))
recCahce = list(map(float, file.readline().split()))

plt.plot(sizes, dist, label = "Полный перебор")
plt.plot(sizes, recCahce, label = "Муравьиный алгоритм")
plt.xlabel("Линейный размер матрицы путей")
plt.ylabel("Время(мс)")
plt.legend()

plt.show()
    
file.close()


