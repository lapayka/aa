#include <iostream>

#include <vector>
#include "matrix.h"
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <time.h>
using namespace std;


struct Params
{
    int TMAX = 1;
    double DEFAULT = 1e-1;
    double B = 0.4;
    double P =  0.5;
};

Params param;

template <typename  Type>
ostream & operator << (ostream &str, const vector<Type> &vec)
{
    for (const auto &elem : vec)
        str << elem << " ";
    str << "\n";
}

int nperm(vector<int> &seq)
{
    int i = seq.size();
    do {
        if (i < 2)
            return EXIT_FAILURE;
        i--;
    } while (seq[i - 1] > seq[i]);

    int j;
    for (j = seq.size(); j > i && seq[i - 1] > seq[--j];) {}

    swap(seq[i - 1], seq[j]);

    for (j = seq.size(); i < --j; i++)
        swap(seq[i], seq[j]);

    return EXIT_SUCCESS;
}



bool check_route(int& len, const Matrix<int> &mat, const vector<int> &route)
{
    int tmp = 0;
    for (int i = 0; i < mat.n() - 1; i++)
    {
        int cur_len;
        if ((cur_len = mat[route[i]][route[i + 1]]) != 0)
            tmp += cur_len;
        else
            return false;
    }
    len = tmp;
    return true;
}

int salesman(vector<int> &min_route, const Matrix<int> &mat)
{
    vector<int> route;
    for (int i = 0; i < mat.n(); i++)
        route.push_back(i);

    int min = INT_MAX;
    int tmp_len;

    do
    {
        if (check_route(tmp_len, mat, route) && min > tmp_len)
        {
                min = tmp_len;
                min_route = route;
        }
    } while(!nperm(route));

    return min;
}

struct Ant
{
    int cur_city;
    vector<int> toVisit;
    int toCount;
    int dst;
    vector<double> probs;
    vector<int> route;
};

void init_pheromones(Matrix<double> &ph)
{
    for (int i = 0; i < ph.n(); i++)
        for (int j = 0; j < ph.m(); j++)
            ph[i][j] = param.DEFAULT;
}

void init_ants(vector<Ant> &ants)
{
    for (int i = 0; i < ants.size(); i++)
    {
        ants[i].cur_city = i;
        ants[i].probs = vector<double>(ants.size() - 1);
    }
}

void init_toVisit_ants(vector<Ant> &ants)
{
    for (int i = 0; i < ants.size(); i++)
    {
        int counter = 0;
        ants[i].toCount = ants.size() - 1;
        ants[i].toVisit.clear();
        ants[i].route.clear();

        ants[i].route.push_back(ants[i].cur_city);

        for (int j = 0; j < ants.size() - 1; j++)
        {
            if (j == ants[i].cur_city)
                counter++;

            ants[i].toVisit.push_back(counter);
            counter++;
        }
    }
}

int antAlg(vector<int> &min_route, const Matrix<int> &mat)
{
    Matrix<double> ph(mat.n(), mat.m());

    int min;

    vector<int> route;
    for (int i = 0; i < mat.n(); i++)
        route.push_back(i);

    while (!check_route(min, mat, route))
        nperm(route);
    double Q = min;

    init_pheromones(ph);

    vector<Ant> ants(mat.n());
    init_ants(ants);

    for (int t = 0; t < param.TMAX; t++)
    {
        init_toVisit_ants(ants);

        for(int i = 0; i < ants.size(); i++)
        {
            for (;ants[i].toCount;)
            {
                double totProb = 0.0;
                for (int j = 0; j < ants[i].toCount; j++)
                {
                    if (mat[ants[i].cur_city][ants[i].toVisit[j]] != 0)
                    {
                        ants[i].probs[j] = powf(1.0 / mat[ants[i].cur_city][ants[i].toVisit[j]], param.B)
                                + powf(1.0 / ph[ants[i].cur_city][ants[i].toVisit[j]], 1 - param.B);
                        totProb += ants[i].probs[j];
                    }
                    else
                        ants[i].probs[j] = 0;
                }

                double r = (double)rand() / (double)RAND_MAX;
                double accum = 0.0;
                int chosen = 0;
                while (chosen < ants[i].toVisit.size() && r >= accum)
                    accum += ants[i].probs[chosen++] / totProb;

                //cout << chosen << " ";

                ants[i].route.push_back(ants[i].toVisit[--chosen]);
                ants[i].cur_city = ants[i].toVisit[chosen];
                ants[i].toVisit.erase(ants[i].toVisit.begin() + chosen);
                //swap(ants[i].toVisit[chosen], ants[i].toVisit[ants[i].toVisit.size() - 1]);
                ants[i].toCount -= 1;
            }

            int len = INT_MAX;
            check_route(len, mat, ants[i].route);
            ants[i].dst = len;
            if (len < min)
            {
                min = len;
                min_route = ants[i].route;
            }
        }

        for (int i = 0; i < ph.n(); i++)
        {
            for (int j = 0; j < ph.m(); j++)
                ph[i][j] *= 1 - param.P;

        }

        for (int i = 0; i < ants.size(); i++)
        {
            for (int j = 0; j < ants[i].route.size() - 1; j++)
            {
                ph[ants[i].route[j]][ants[i].route[j + 1]] = Q / ants[i].dst;
            }
        }

        for (int i = 0; i < ph.n(); i++)
        {
            for (int j = 0; j < ph.m(); j++)
                if (ph[i][j] < param.DEFAULT)
                    ph[i][j] = param.DEFAULT;
        }
    }

    return min;
}


void tests(int (*func)(vector<int> &, const Matrix<int> &))
{
    vector<int> route;
    Matrix<int> mat({{0, 3, 1, 6, 8},
                    {3, 0, 4, 1, 0},
                    {1, 4, 0, 5, 0},
                    {6, 1, 5, 0, 1},
                    {8, 0, 0, 1, 0}});

    cout << "Matrix1\n" << mat;
    cout << func(route, mat) <<"\n" << route << "\n";

    Matrix<int> mat2({{0, 10, 15, 20},
                {10, 0, 35, 25},
                {15, 35, 0, 30},
                {20, 25, 30,0}});

    cout << "Matrix2\n" << mat2;
    cout << func(route, mat2)<<"\n" << route << "\n";
}


#define TIMECOUNT 10

double time(const Matrix<int> &mat, int (*func)(vector<int> &, const Matrix<int> &))
{
    vector<int> route;
    clock_t start = clock();
    for (int i =0; i < TIMECOUNT; i++)
    {
        func(route, mat);
    }
    clock_t end = clock() - start;

    return (double)end / TIMECOUNT;
}

#define ARRCOUNT 9

void find_params()
{
    Matrix<int> m(9, 9);

    ifstream str("../input1.txt");
    str >> m;
    str.close();

    int len;
    vector<int> route;
    len = salesman(route, m);


    int cur_len;

    do
    {
        route.clear();
        cur_len = antAlg(route, m);

        cout << "\t" << param.TMAX << " & " << param.B << " & " << param.P << " & " << cur_len << " & " << len << "\\\\\\hline\n";

        if (cur_len != len)
        {
            if (param.P <= 0.7)
            {
                param.P += 0.1;
            }
            else
            {
                param.P = 0.1;
                if (param.B <= 0.7)
                    param.B += 0.1;
                else
                {
                    param.B = 0.1;
                    param.TMAX += 1;
                }
            }
        }



    } while (cur_len > len);
}

int main()
{
    setbuf(stdout, NULL);
    vector<int> vec;
    Matrix<int> mat = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};

    find_params();

    tests(salesman);
    tests(antAlg);

    ifstream str("../input.txt");

    Matrix<int> arr[ARRCOUNT];
    for (int i = 0; i < ARRCOUNT; i++)
    {
        str >> arr[i];
        //cout << arr[i];
    }
    str.close();

    ofstream os("../times.txt");
    for (int i = 0; i < ARRCOUNT; i++)
    {
       os << time(arr[i], salesman) << " ";
    }
    os << "\n";
    for (int i = 0; i < ARRCOUNT; i++)
    {
        os << time(arr[i], antAlg) << " ";
    }
    str.close();


}
