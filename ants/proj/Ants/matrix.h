#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <initializer_list>
#include <iostream>

using namespace std;

template <typename Type>
class Matrix
{
private:
    int _n, _m;
    Type **matrix;
public:
    Matrix() = default;
    Matrix(int n, int m);
    Matrix(const Matrix &matrix);
    Matrix(const initializer_list<initializer_list<Type>> & list)
        :
          _n(list.size()),
          _m(list.begin()->size())
    {
        matrix = nullptr;
        matrix = new Type*[_n];
        *matrix = nullptr;
        *matrix = new Type[_n * _m];

        for (int i = 1; i < _n; i++)
            matrix[i] = matrix[i - 1] + _m;

        int i = 0;
        for (const auto &l : list)
        {
            int j = 0;
            for (const auto &elem : l)
            {
                matrix[i][j] = elem;
                j++;
            }
            i++;
        }

    }
    ~Matrix();

    int n() const {return _n; }
    int m() const  {return _m; }

    Type &operator ()(int i, int j) {return matrix[i][j]; };
    Type* operator [](int i){return matrix[i]; };
    const Type* operator [](int i) const {return matrix[i]; };

    Type *getArray() {return matrix[0]; };
    constexpr Matrix<Type> &operator = (const Matrix<int> &mat)
    {
        _n = mat._n;
        _m = mat._m;
        matrix = nullptr;
        matrix = new Type*[_n];
        *matrix = nullptr;
        *matrix = new Type[_n * _m];

        memcpy(getArray(), mat.matrix[0], sizeof (Type) * _n * _m);

        for (int i = 1; i < _n; i++)
            matrix[i] = matrix[i - 1] + _m;
    }
};

template<typename Type>
Matrix<Type>::Matrix(int n, int m)
    :
      _n(n),
      _m(m)
{
    matrix = nullptr;
    matrix = new Type*[n];
    *matrix = nullptr;
    *matrix = new Type[n * m];

    for (int i = 1; i < n; i++)
        matrix[i] = matrix[i - 1] + m;
}

template<typename Type>
Matrix<Type>::Matrix(const Matrix &_matrix)
{
    _n = _matrix._n;
    _m = _matrix._m;
    matrix = nullptr;
    matrix = new Type*[_n];
    *matrix = nullptr;
    *matrix = new Type[_n * _m];

    memcpy(matrix[0], _matrix.matrix[0], sizeof (Type) * _n * _m);

    for (int i = 1; i < _n; i++)
        matrix[i] = matrix[i - 1] + _m;
}

template<typename Type>
Matrix<Type>::~Matrix()
{
    if (matrix != nullptr)
    {
        if (*matrix != nullptr)
            delete [] *matrix;
        delete [] matrix;
        matrix = nullptr;
    }
}

template<typename Type>
ostream & operator << (ostream & str, const Matrix<Type> &mat)
{
    for (int i = 0; i < mat.n(); i++)
    {
        for (int j = 0; j < mat.m(); j++)
            str << mat[i][j] << " ";
        str << "\n";
    }

    return str;
}

template<typename Type>
istream & operator >> (istream & str, Matrix<Type> &mat)
{
    int n, m;
    str >> n >> m;
    mat = Matrix<Type>(n, m);
    for (int i = 0; i < mat.n(); i++)
    {
        for (int j = 0; j < mat.m(); j++)
            str >> mat[i][j];
    }

    return str;
}

#endif // MATRIX_H
