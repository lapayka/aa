from random import *

sizes = [i for i in range(9, 10, 1)]

file = open("input1.txt", "w")

for i in sizes:
    file.write(str(i) + " " + str(i) + "\n")
    matrix1 = [[0 for j in range(i)] for z in range(i)]
    for j in range(1, i):
        for z in range(0, j):
            matrix1[j][z] = randint(0, 50)
            matrix1[z][j] = matrix1[j][z]

    for j in range(i):
        for k in range (i):
            if k != i-1:
                file.write(str(matrix1[j][k]) + " ")
            else:
                file.write(str(matrix1[j][k]) + "\n")
file.close()
