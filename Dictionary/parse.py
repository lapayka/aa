import json

file = open("input.txt", "w")

rfile = open("ships.csv")

string = "  "
    
i = 0

while(string != ""):
    string = rfile.readline()

    a = string.split(',')
    if len(a) < 2:
        break;
    k = a[1].replace(':', '')
    k.replace('\n', '')
    d = a[2].replace('\n', '')
    file.write(k + ":" + d + "\n")

file.close()
