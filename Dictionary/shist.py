import json
import matplotlib.pyplot as plt

def cmp(c):
    return c[1]

def create_hist(file_name):
    file = open(file_name, "r")

    a = list(map(int, file.readline().split()))
    x = list(range(len(a)))

    a = sorted(a)

    plt.hist(x, bins = len(x), weights = a)
    plt.xlabel("Индекс ключа")
    plt.ylabel("Количество сравнений")

    plt.show()


    file.close()


create_hist("comps1.txt")
create_hist("comps2.txt")

create_hist("comps3.txt")

