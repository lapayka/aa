#ifndef PARTDICT_H
#define PARTDICT_H

#include <string>
#include <vector>


#include "basedict.h"

enum{ ALPHABET_COUNT = 26};

#include "dict.h"

using namespace std;

class PartDict : public BaseDict
{
    vector<Dict> dict;
    char min;
    char max;
public:
    PartDict()
    {
        min = 'A';
        for (int i = 0; i < ALPHABET_COUNT; i++)
            dict.push_back(Dict());

    }

    virtual void add(const pair<string, string> &par) override
    {
        int tmp = par.first[0] - min;
        dict[tmp].add(par);
    }
    virtual bool find(string &result, const string &toF, int &compCount) override
    {
        int tmp = toF[0] - min;
        compCount = 0;

        //if (tmp < 0 || tmp >= dict.size())
        //    return false;

        int buf;

        bool f = dict[tmp].find(result, toF, buf);

        compCount += buf;

        return f;
    }

    virtual void stats(ofstream &str) override
    {
        string buf;
        int cmp;
        for (auto &a : dict)
        {
            for (auto &elem : a)
            {
                this->find(buf, elem.first, cmp);
                str << cmp << " ";
            }
        }
        str << "\n";
    }
};

#endif // PARTDICT_H
