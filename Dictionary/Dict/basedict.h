#ifndef BASEDICT_H
#define BASEDICT_H

#include <string>
#include <vector>
#include <fstream>

using namespace std;

class BaseDict
{
protected:
    bool readLine(istream &str, pair<string, string> &out)
    {
        string tmp1;
        string tmp2;


        char buf[1024];

        str.getline(buf, 1024);

        int i = 0;

        for (; buf[i] != ':' && buf[i] != '\n' && buf[i]; i++)
        {
            tmp1.push_back(buf[i]);
        }

        i++;

        for (; buf[i] != '\n' && buf[i]; i++)
        {
            tmp2.push_back(buf[i]);
        }

        while (tmp1[tmp1.size() - 1] == ' ')
        {
            tmp1.resize(tmp1.size() - 1);
        }


        out = make_pair(tmp1, tmp2);

        return tmp1 != "";
    };

public:
    BaseDict();

    virtual void add(const pair<string, string> &par) = 0;
    virtual bool find(string &result, const string &toF, int &compCount) = 0;
    virtual void load(istream &str)
    {
       pair<string, string> tmp;
       while (readLine(str, tmp))
           add(tmp);
    };

    virtual void stats(ofstream &str) = 0;
};

//void operator = (pair<string, string> & lhs, const pair<string, string> & rhs); {lhs.first = rhs.first; lhs.second = rhs.second; }

#endif // BASEDICT_H
