#ifndef DICT_H
#define DICT_H

#include <string>
#include <vector>
#include "basedict.h"
#include <iostream>

#include <clocale>

using namespace std;

class Dict : public BaseDict
{
    vector<pair<string, string>> dict;

private:
    void insert(const pair<string, string> &par)
    {
        for (int i = 0; i < dict.size(); i++)
        {
            for (auto elem = dict.begin(); elem < dict.begin(); ++elem)
            {
                if (elem->first > par.first)
                {
                    dict.insert(elem, par);
                    return;
                }
            }
        }

        dict.push_back(par);
    }

public:
    Dict();


    virtual void add(const pair<string, string> &par) override {insert(par); }

    virtual bool find(string &result, const string &toF, int &compCount) override
    {
        compCount = 1;
        if (dict.size() == 0)
            return false;
        int left = 0;
        int right = dict.size() - 1;

        compCount += 1;
        if (dict[left].first == toF)
        {
            result = dict[left].second;
            return true;
        }

        compCount++;
        if (dict[right].first == toF)
        {
            result = dict[right].second;
            return true;
        }

        while (left < right)
        {
            compCount++;
            int middle = (left + right) / 2;
            //std::cout << dict[middle].first << "\n";
            if (dict[middle].first == toF)
            {
                result = dict[middle].second;
                return true;
            }            

            compCount++;
            if (dict[middle].first < toF)
            {
                left = middle + 1;
            }
            else
            {
                right = middle;
            }
        }

        return false;
    }

    auto begin() {return dict.begin(); }
    auto end() {return dict.end(); }

    virtual void stats(ofstream &str) override
    {
        for (const auto &elem : dict)
        {
            int comp;
            string buf;
            this->find(buf, elem.first, comp);
            str << comp << " ";
        }

        str << "\n";
    }
};

#endif // DICT_H
