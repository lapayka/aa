#ifndef SIMDICT_H
#define SIMDICT_H

using namespace std;

#include <string>
#include <vector>

#include "basedict.h"

class SimDict : public BaseDict
{
    vector<pair<string, string>> dict;


public:
    SimDict();

    virtual void add(const pair<string, string> &par) override {dict.push_back(par); }
    virtual bool find(string &result, const string &toF, int &compCount) override
    {
        compCount = 0;
        for (int i = 0; i < dict.size(); i++)
        {
            compCount++;
            if (dict[i].first == toF)
            {
                result = dict[i].second;
                return true;
            }
        }
        return false;
    }

    virtual void stats(ofstream &str) override
    {
        for (const auto &elem : dict)
        {
            int comp;
            string buf;
            this->find(buf, elem.first, comp);
            str << comp << " ";
        }

        str << "\n";
    }
};

#endif // SIMDICT_H
