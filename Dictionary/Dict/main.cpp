#include <iostream>

using namespace std;

#include "dict.h"
#include "simdict.h"
#include "partdict.h"

void make_tests(BaseDict &dict)
{
    cout << "test1:\n";

    string tmp;
    int compCount;
    cout << dict.find(tmp, string("Bumper"), compCount) << " " << tmp << "\n";

    cout << "test2:\n";

    cout << dict.find(tmp, string("Z43"), compCount) << " " << tmp << "\n";

    cout << "test3:\n";

    cout << dict.find(tmp, string("Random"), compCount) << " " << tmp << "\n";

    cout << "---------------\n";
}

void tests()
{
    Dict dict;
    PartDict pDict;
    SimDict sDict;

    ifstream str("../input.txt");
    dict.load(str);
    str.close();

    ifstream str1("../input.txt");
    pDict.load(str1);
    str.close();

    ifstream str2("../input.txt");
    sDict.load(str2);
    str.close();

    make_tests(dict);
    make_tests(pDict);
    make_tests(sDict);
}

void stats()
{
    Dict dict;
    PartDict pDict;
    SimDict sDict;

    ifstream str("../input.txt");
    dict.load(str);
    str.close();

    ifstream str1("../input.txt");
    pDict.load(str1);
    str.close();

    ifstream str2("../input.txt");
    sDict.load(str2);
    str.close();


    ofstream f1("../comps1.txt");
    ofstream f2("../comps2.txt");
    ofstream f3("../comps3.txt");

    sDict.stats(f1);
    dict.stats(f2);
    pDict.stats(f3);

    f3.close();
    f2.close();
    f1.close();
}

int main()
{
    tests();

    stats();


    return 0;
}
