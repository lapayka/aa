TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        basedict.cpp \
        dict.cpp \
        main.cpp \
        partdict.cpp \
        simdict.cpp

HEADERS += \
    basedict.h \
    dict.h \
    partdict.h \
    simdict.h
