#ifndef ARRAY_CPP
#define ARRAY_CPP
#include "array.h"

void Array::bubble_sort()
{
    for (int i = 1; i < _size - 1; i++)
        for (int j = _size - 1; j >= i; j--)
            if (arr[j] < arr[j - 1])
                swap(arr[j], arr[j - 1]);
}

void Array::select_sort()
{
    for (int i = 0; i < _size; i++)
    {
        int min_index = i;
        for (int j = i + 1; j < _size; j++)
            if (arr[j] < arr[min_index])
                min_index = j;
        if (min_index != i)
            swap(arr[i], arr[min_index]);
    }
}

void Array::qsort(int *arr, int left, int right)
{
    if (left >= right)
        return;
    int i = left;
    int j = right;

    int middle = arr[(left + right) / 2];

    while (i <= j)
    {
        while (arr[i] < middle)
            i++;
        while (arr[j] > middle)
            j--;

        if (i <= j)
        {
            swap(arr[i], arr[j]);
            i++;
            j--;
        }
    }

    if (j > 0)
        qsort(arr, left, j);
    if (i <= right)
        qsort(arr, i, right);
}

ostream & operator << (ostream &stream, const Array &arr)
{
    for (int i = 0; i < arr.size(); i++)
        stream << arr[i] << " ";
    stream << "\n";

    return stream;
}

istream & operator >> (istream &stream, Array &arr)
{
    int *buf;
    int n;

    stream >> n;

    buf = (int*)malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++)
        stream >> buf[i];

    arr = Array(buf, n);

    free(buf);

    return stream;
}

#endif // ARRAY_CPP
