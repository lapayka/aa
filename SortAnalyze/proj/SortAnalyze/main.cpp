#include <iostream>
#include <initializer_list>
#include <time.h>
#include <istream>
#include <fstream>
#include "array.h"

enum {ARR_COUNT = 10};

using namespace std;


double timeMeasure(const Array &arr, void (Array::*sort)())
{
    int measurmentsCount = 1000;

    clock_t time = 0;



    for (int i = 0; i < measurmentsCount; i++)
    {
        Array tmp(arr);
        clock_t sTime = clock();
        (tmp.*sort)();
        time += clock() - sTime;
    }

    return (double)(time) / measurmentsCount ;
}

void timesMeasure(double times[], const Array arr[], int n,  void (Array::*sort)())
{
    for (int i = 0; i < n; i++)
        times[i] = timeMeasure(arr[i], sort);
}

void readArrays(istream &stream, Array arrays[], int count)
{
    for (int i = 0; i < count; i++)
        stream >> arrays[i];
}

void outputArray(ostream &stream, double arr[], int size)
{
    for (int i = 0; i < size; i++)
        stream << arr[i] << " ";
    stream << "\n";
}

void make_test(const Array &arr)
{
    {
        Array b_tmp(arr);
        Array s_tmp(arr);
        Array q_tmp(arr);

        b_tmp.bubble_sort();
        s_tmp.select_sort();
        q_tmp.quick_sort();

        cout << "Test array: " << arr;
        cout << "Bubble sort: " << b_tmp;
        cout << "Select sort: " << s_tmp;
        cout << "Quick sort: " << q_tmp;
        cout << "-----------\n";
    }
}

void make_tests()
{
    setbuf(stdout, NULL);

    Array test_arr1({1, 2, 3, 4, 5});
    Array test_arr2({-1, -2, -3, -4, -5});
    Array test_arr3({5, 3, 4, 1, 2, -4});
    Array test_arr4({1});

    make_test(test_arr1);
    make_test(test_arr2);
    make_test(test_arr3);
    make_test(test_arr4);

}

int main()
{

    make_tests();


    Array direct[ARR_COUNT];
    Array random[ARR_COUNT];
    Array reversed[ARR_COUNT];

    ifstream istream("arrays.txt");
    if (istream.is_open())
    {
        readArrays(istream, direct, ARR_COUNT);
        readArrays(istream, random, ARR_COUNT);
        readArrays(istream, reversed, ARR_COUNT);

        istream.close();

        //for (int i = 0; i  < ARR_COUNT; i++)
        //{
        //    cout << direct[i] << random[i] << reversed[i];
        //}

        double bestBubble[ARR_COUNT];
        double randomBubble[ARR_COUNT];
        double worstBubble[ARR_COUNT];

        double bestSelect[ARR_COUNT];
        double randomSelect[ARR_COUNT];
        double worstSelect[ARR_COUNT];

        double bestQuick[ARR_COUNT];
        double randomQuick[ARR_COUNT];
        double worstQuick[ARR_COUNT];

        timesMeasure(bestBubble, direct, ARR_COUNT, &Array::bubble_sort);
        timesMeasure(randomBubble, random, ARR_COUNT, &Array::bubble_sort);
        timesMeasure(worstBubble, reversed, ARR_COUNT, &Array::bubble_sort);

        timesMeasure(bestSelect, direct, ARR_COUNT, &Array::select_sort);
        timesMeasure(randomSelect, random, ARR_COUNT, &Array::select_sort);
        timesMeasure(worstSelect, reversed, ARR_COUNT, &Array::select_sort);

        timesMeasure(bestQuick, direct, ARR_COUNT, &Array::quick_sort);
        timesMeasure(randomQuick, random, ARR_COUNT, &Array::quick_sort);
        timesMeasure(worstQuick, random, ARR_COUNT, &Array::quick_sort);

        ofstream ostream("times.txt");

        outputArray(ostream, bestBubble, ARR_COUNT);
        outputArray(ostream, bestSelect, ARR_COUNT);
        outputArray(ostream, bestQuick, ARR_COUNT);

        outputArray(ostream, worstBubble, ARR_COUNT);
        outputArray(ostream, worstSelect, ARR_COUNT);
        outputArray(ostream, worstQuick, ARR_COUNT);

        outputArray(ostream, randomBubble, ARR_COUNT);
        outputArray(ostream, randomSelect, ARR_COUNT);
        outputArray(ostream, randomQuick, ARR_COUNT);

        ostream.close();
    }

    return 0;
}
