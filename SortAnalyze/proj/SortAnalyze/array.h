#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>
#include <initializer_list>

using namespace std;

class Array
{
    int *arr = NULL;
    int _size = 0;

public:
    Array() = default;

    explicit Array(int n) {arr = (int *)malloc(sizeof(int) * n); _size = n; }

    Array(const int * a, int n) {
                                    arr = (int *)malloc(sizeof(int) * n);
                                    _size = n;
                                    for (int i = 0; i < n; i++)
                                        arr[i] = a[i];
                                }
    Array(const initializer_list<int> &list) {
                                              arr = (int *)malloc(sizeof(int) * (_size = list.size()));
                                              int i = 0;
                                              for (auto el : list)
                                              arr[i++] = el;
                                              }

    Array(const Array &copy) {
                              _size = copy._size;
                              arr = (int *)malloc(sizeof(int) * _size);
                              for (int i = 0; i < _size; i++)
                                  arr[i] = copy.arr[i];
                             }

    Array(Array &&copy)      {
                              _size = copy._size;
                              copy._size = 0;
                              arr = copy.arr;
                              copy.arr = NULL;
                             }

    void operator =(Array &&copy)
                             {
                                 _size = copy._size;
                                 copy._size = 0;
                                 arr = copy.arr;
                                 copy.arr = NULL;
                             }

    ~Array() {free(arr); arr = NULL; }

    int size() const {return _size; }
    int operator [](int index) const {return arr[index]; }

    void bubble_sort();
    void select_sort();
    void quick_sort() { qsort(arr, 0, _size - 1);};

private:
    void qsort(int *arr, int left, int right);
    void swap(int &a, int &b) {int buf = a; a = b; b = buf; }
};

ostream &operator <<(ostream & stream, const Array &arr);
istream &operator >>(istream & stream, Array &arr);

#endif // ARRAY_H
